package com.main.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/time")
public class Time {

	@RequestMapping(value = "/UTC", method = RequestMethod.GET)
	public String getUTCTime(){
		OffsetDateTime now = OffsetDateTime.now( ZoneOffset.UTC );
		
		
		return now.toString();
	} 
	
	@RequestMapping(value="/PST", method = RequestMethod.GET)
	public String getPSTTime(){
		Date today = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		sdf.setTimeZone(TimeZone.getTimeZone("PST"));
		
		return sdf.format(today);
	}
}
